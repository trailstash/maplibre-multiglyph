export class Protocol {
  constructor(glyphUrls, name = "glyphs") {
    this.glyphUrls = glyphUrls;
    this.name = name;
    this.url = name + "://{fontstack}/{range}";
  }
  fetchGlyphs = async (urls, fontstack, range) => {
    let resp;
    for (const url of urls) {
      resp = await fetch(
        url.replace("{fontstack}", fontstack).replace("{range}", range)
      );
      if (resp.status === 200) {
        return resp;
      }
    }
    return resp;
  };
  tile = (params, callback) => {
    const url = new URL(params.url);
    if (url.protocol === `${name}:`) {
      const [fontstack, range] = url.pathname.slice(2).split("/");
      this.fetchGlyphs(this.glyphUrls, fontstack, range)
        .then((resp) => resp.arrayBuffer())
        .then((ab) => callback(null, new Uint8Array(ab), null, null));
    }
    return {
      cancel: () => {},
    };
  };
}
