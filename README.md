# `@trailstash/maplibre-multiglyph`

This is a little library to enable MapLibre to use multiple
[`glyphs`](https://maplibre.org/maplibre-style-spec/glyphs/) endpoints.

[Demo](https://trailstash.gitlab.io/maplibre-multiglyph/)

### Usage

#### Import

```
import { Protocol } from "@trailstash/maplibre-multiglyph";
```

#### Configure multiple font stacks

This configures MapLibre to load fonts from OSM Americana's
[fontstack66](https://github.com/osm-americana/fontstack66) and
[openmaptiles-fonts](https://github.com/TrailStash/openmaptiles-fonts). This reflects how you might
use the library when providing MapLibre with a style object.

```
const protocol = new Protocol([
  "https://osm-americana.github.io/fontstack66/{fontstack}/{range}.pbf",
  "https://trailstash.github.io/openmaptiles-fonts/fonts/{fontstack}/{range}.pbf",
]);
maplibregl.addProtocol(protocol.name, protocol.tile);
```

Then use `protocol.url` as the value for the `glyphs` key in your style object.

#### Adding a second font stack to a `style.json` after it's loaded

Here is an example of adding glyphs to the a style at runtime, similar to adding images to using
`map.loadImage` to augment a style's provided sprites. This is useful if you're loading 3rd party
styles and want to add layers using fonts not provided in the style.

```
map.on("style.load", () => {
  const protocol = new Protocol([
    map.getGlyphs(),
    "https://trailstash.github.io/openmaptiles-fonts/fonts/{fontstack}/{range}.pbf",
  ]);
  maplibregl.addProtocol(protocol.name, protocol.tile);
  map.setGlyphs(protocol.url);
});
```
